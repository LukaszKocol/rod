﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : MonoBehaviour
{
    private Transform playerTransform;
    private Vector3 cameraPosition;
    // Start is called before the first frame update
    void Start()
    {
        cameraPosition = new Vector3();

        GameObject existingPlayer = GameObject.Find("Player(Clone)");

        if (existingPlayer == null)
            existingPlayer = GameObject.Find("Player");
        
        playerTransform = existingPlayer.transform;
    }

    // Update is called once per frame
    void Update()
    {
        cameraPosition =  playerTransform.position;
        cameraPosition.z = transform.position.z;
        transform.position = cameraPosition;
    }
}
