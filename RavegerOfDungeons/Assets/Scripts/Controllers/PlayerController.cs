﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public Rigidbody2D theRB;
    public float activeMoveSpeed = 5;

    public Animator myAnim;
    public Animator wpnAnim;
    public Transform wpnTransform;
    public Vector2 moveDir;
    private Vector2 position;
    private bool canMove = true;

    private void Awake()
    {
        UIManager.OnHideShop += StartPlayer;
        UIManager.OnShowShop += StopPlayer;
        TileMapGenerator.OnGenerateLevel += SetPositon;
    }

    // Start is called before the first frame update
    void Start()
    {
        position = new Vector2();
        moveDir = new Vector2();
        //DontDestroyOnLoad(gameObject);
    }

    private void OnDisable()
    {
        UIManager.OnHideShop -= StartPlayer;
        UIManager.OnShowShop -= StopPlayer;
        TileMapGenerator.OnGenerateLevel -= SetPositon;
    }
    void SetPositon(int x, int y)
    {
        position.x = x;
        position.y = y;
        this.transform.position = position;
    }

    private void StopPlayer()
    {
        canMove = false;
    }

    private void StartPlayer()
    {
        canMove = true;
    }
    // Update is called once per frame
    void Update()
    {
        if (!canMove)
            return;

        moveDir.x = Input.GetAxisRaw("Horizontal");
        moveDir.y = Input.GetAxisRaw("Vertical");
        theRB.velocity = moveDir * activeMoveSpeed;
        
        myAnim.SetFloat("moveX", theRB.velocity.x);
        myAnim.SetFloat("moveY", theRB.velocity.y);

        if (Input.GetAxisRaw("Horizontal") == 1 || Input.GetAxisRaw("Horizontal") == -1 || Input.GetAxisRaw("Vertical") == 1 || Input.GetAxisRaw("Vertical") == -1)
        {
            myAnim.SetFloat("lastMoveX", Input.GetAxisRaw("Horizontal"));
            myAnim.SetFloat("lastMoveY", Input.GetAxisRaw("Vertical"));
        }

        if (theRB.velocity != Vector2.zero)
        {
            if (Input.GetAxisRaw("Horizontal") != 0)
            {
                if (Input.GetAxisRaw("Horizontal") < 0)
                {
                    wpnTransform.rotation = Quaternion.Euler(0f, 0f, 180f);
                }
                else
                {
                    wpnTransform.rotation = Quaternion.Euler(0f, 0f, 0f);
                }
            }

            if (Input.GetAxisRaw("Vertical") != 0)
            {
                if (Input.GetAxisRaw("Vertical") < 0)
                {
                    wpnTransform.rotation = Quaternion.Euler(0f, 0f, -90f);
                }
                else
                {
                    wpnTransform.rotation = Quaternion.Euler(0f, 0f, 90f);
                }
            }
        }

        if (Input.GetMouseButtonDown(0))
        {
            wpnAnim.SetTrigger("Attack");
        }
    }
}
