﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

public class WeaponController : MonoBehaviour
{
    public int damageToDeal;
    public PlayerManager playerManager;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Enemy")
        {
            var enemyStats = other.gameObject.GetComponent<EnemyStats>();
            int damageToDeal = CalculateAttack(enemyStats.defense);
            enemyStats.DamageEnemy(damageToDeal);
            other.gameObject.GetComponent<EnemyManager>().enemyController.gotHitted = true;
            TextRendererParticleSystem.instance.SpawnParticle(other.gameObject.transform.position, damageToDeal.ToString(), Color.green, 1);
        }
    }

    private int CalculateAttack(int targetDefense)
    {
        return playerManager.CalculateDamage(targetDefense);
    }

}
