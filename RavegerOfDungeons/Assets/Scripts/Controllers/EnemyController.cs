﻿using System.Collections;
using System;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    private Transform targetTransform;
    private bool targetHited = false;
    public bool gotHitted = false;
    public static bool isInShop = false;
    public Rigidbody2D theRB;
    public float moveTime, waitTime;
    public Vector2 moveDir;
    public float moveSpeed;
    public float rangeToChase;
    public bool shouldChase;
    public bool canMove = false;
    private StateMachine stateMachine;
    Idle wait;
    Move move;
    Chase chase;
    WaitAfterHit waitAfterHit;
    Hited hited;
    InShop inShop;
    // Start is called before the first frame update
    void Start()
    {
        stateMachine = new StateMachine();
        wait = new Idle(theRB);
        move = new Move(theRB);
        chase = new Chase(theRB);
        waitAfterHit = new WaitAfterHit();
        hited = new Hited(theRB);
        inShop = new InShop();
        stateMachine.AddTransition(wait, move, Wait());
        stateMachine.AddTransition(move, wait, Move());
        stateMachine.AddTransition(chase, move, StopChase());
        stateMachine.AddTransition(wait, chase, Chase());
        stateMachine.AddTransition(move, chase, Chase());
        stateMachine.AddTransition(chase, waitAfterHit, HitTarget());
        stateMachine.AddTransition(waitAfterHit, wait, WaitAfterHit());
        stateMachine.AddTransition(chase, hited, GotHited());
        stateMachine.AddTransition(hited, move, WaitAfterHited());
        stateMachine.AddTransition(inShop, move, OnLeaveShop());
        moveDir = new Vector2();
        stateMachine.SetState(wait);

        Func<bool> Wait() => WaitToMove;
        Func<bool> Move() => MoveToWait;
        Func<bool> StopChase() => () => targetTransform != null && Vector3.Distance(transform.position, targetTransform.position) > rangeToChase;
        Func<bool> Chase() => () => targetTransform != null && Vector3.Distance(transform.position, targetTransform.position) <= rangeToChase && (targetHited = false) == false;
        Func<bool> HitTarget() => () => targetHited;
        Func<bool> WaitAfterHit() => () => waitAfterHit.waitAfterHit < 0f;
        Func<bool> GotHited() => () => gotHitted;
        Func<bool> WaitAfterHited() => () => hited.waitAfterHited < 0;
        Func<bool> OnLeaveShop() => () => canMove;
    }

    private bool WaitToMove()
    {
        return wait.waitToMove <= 0;
    }


    private bool MoveToWait()
    {
        gotHitted = false;
        return move.moveCounter <= 0;
    }
    private void OnDisable()
    {
        canMove = false;
    }

    // Update is called once per frame
    void Update()
    {
        if(canMove)
            stateMachine.Tick();
    }

    public void StartChaseTarget(Transform playerTransform)
    {
        if (chase != null)
        {
            targetTransform = playerTransform;
            chase.SetTransforms(playerTransform, transform);
        }
    }

    public bool CheckChaseState()
    {
        if (stateMachine.CheckChaseState())
        {
            targetHited = true;
            return true;
        }
        return false;
    }
}
