﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EssentialsLoader : MonoBehaviour
{
    public GameObject player;
    // Start is called before the first frame update
    void Start()
    {
        GameObject existingPlayer = GameObject.Find("Player(Clone)");

        if (existingPlayer == null)
            existingPlayer = GameObject.Find("Player");

        if (existingPlayer == null)
        {
            Instantiate(player);
        }

    }
}
