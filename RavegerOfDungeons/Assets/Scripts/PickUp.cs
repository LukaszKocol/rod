﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickUp : MonoBehaviour
{
    public static event System.Action<int> OnPickUp = delegate { };

    private int value;
    // Start is called before the first frame update
    void Start()
    {
        value = Random.Range(10, 15);
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.tag == "Player")
        {
            OnPickUp(value);
            DisablePickUp();
        }
    }

    private void DisablePickUp()
    {
        Destroy(this.gameObject);
    }
}
