﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class EnemiesChecker : MonoBehaviour
{
    public static event System.Action OnLoadShop = delegate { };

    public List<GameObject> enemies;
    private List<GameObject> currentLevelEnemies;
    private Vector2 position;
    private bool loadShop = false;
    private int currentLevelEnemiesCount = 10;

    private void Awake()
    {
        currentLevelEnemies = new List<GameObject>();
        EnemyStats.OnEnemyDies += OnEnemyDies;
        TileMapGenerator.OnGeenrateEnemiesPosition += SetPosition;
        GameManager.OntimeOut += OnTimeOut;
        UIManager.OnHideShop += ActivateEnemies;
        UIManager.OnShowShop += DeactivateEnemies;
    }
    // Start is called before the first frame update
    void Start()
    {
        position = new Vector2();
    }

    private void LoadEnemies()
    {
        for (int i = 0; i < currentLevelEnemiesCount; i++)
        {
            currentLevelEnemies.Add(enemies[i]);
        }

        //currentLevelEnemies.Add(enemies[0]);
    }

    private void SetPosition(int[] map, int sizeX, int sizeY, int maxSizeY)
    {
        ReloadEnemies();
        int x;
        int y;

        foreach (var enemy in currentLevelEnemies)
        {
            enemy.gameObject.SetActive(true);
            while (true)
            {
                x = Random.Range(0, sizeX);
                y = Random.Range(0, sizeY);
                if (map[x + y * maxSizeY] == 1)
                {
                    position.x = x;
                    position.y = y;
                    enemy.transform.position = position;
                    break;
                }
            }
        }
    }

    private void OnDisable()
    {
        EnemyStats.OnEnemyDies -= OnEnemyDies;
        TileMapGenerator.OnGeenrateEnemiesPosition -= SetPosition;
        GameManager.OntimeOut -= OnTimeOut;
        UIManager.OnHideShop -= ActivateEnemies;
        UIManager.OnShowShop -= DeactivateEnemies;
    }

    private void DeactivateEnemies()
    {
        foreach (var enemy in enemies)
            enemy.GetComponent<EnemyManager>().DeactivateEnemy();
    }

    private void ActivateEnemies()
    {
        ReloadEnemies();

        foreach (var enemy in currentLevelEnemies)
            enemy.GetComponent<EnemyManager>().ActivateEnemy();

    }

    // Update is called once per frame
    void Update()
    {
        if (loadShop)
        {
            OnLoadShop();
            loadShop = false;
        }
    }

    private void ReloadEnemies()
    {
        currentLevelEnemiesCount += 10;

        if (currentLevelEnemiesCount > 200)
            currentLevelEnemiesCount = 200;

        currentLevelEnemies.Clear();
        LoadEnemies();
    }

    public void OnEnemyDies()
    {
        loadShop = true;
        foreach (var enemy in currentLevelEnemies)
        {
            if (enemy.activeInHierarchy)
            {
                loadShop = false;
                break;
            }
        }
    }

    public void OnTimeOut()
    {
        TileMapGenerator.OnGeenrateEnemiesPosition -= SetPosition;
    }
}
