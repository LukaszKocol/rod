﻿using UnityEngine;
using TMPro;
using Utilities;
using System;

public class ShopManager : MonoBehaviour
{
    private PlayerStatistics playerStatistics;
    public TMP_Text currentExp;
    public TMP_Text totalExp;
    public TMP_Text attack;
    public TMP_Text defense;
    public TMP_Text attackReqText;
    public TMP_Text defenseReqText;

    private float[] requirementArray;
    private const int attackReq = 0;
    private const int defenseReq = 1;
    private const string expReq = "Exp to upgrade: ";
    private void LoadPlayerStats(PlayerStatistics playerStatistics)
    {
        this.playerStatistics = playerStatistics;
        UpdateShop();
    }
    private void Awake()
    {
        PlayerManager.OnLoadStats += LoadPlayerStats;
        requirementArray = new float[5];
        for (int i = 0; i < 5; i++)
        {
            requirementArray[i] = 4;
        }
    }

    private void OnDestroy()
    {
        PlayerManager.OnLoadStats -= LoadPlayerStats;
    }
    public void UpdateShop()
    {
        currentExp.text = playerStatistics.currentExp.ToString();
        totalExp.text = playerStatistics.totalExp.ToString();
        attack.text = playerStatistics.attack.ToString();
        defense.text = playerStatistics.defense.ToString();
        attackReqText.text = expReq + ((int)requirementArray[attackReq]).ToString();
        defenseReqText.text = expReq + ((int)requirementArray[defenseReq]).ToString();
    }

    public void OnAttackUpgrade()
    {
        if (CheckExpRequirement(attackReq))
        {
            playerStatistics.attack++;
            attack.text = playerStatistics.attack.ToString();
            playerStatistics.currentExp -= (int)requirementArray[attackReq];
            currentExp.text = playerStatistics.currentExp.ToString();
            requirementArray[attackReq] = requirementArray[attackReq] * 1.1f;
            attackReqText.text = expReq + ((int)requirementArray[attackReq]).ToString();
        }
    }

    public void OnDefenseUpgrade()
    {
        if (CheckExpRequirement(defenseReq))
        {
            playerStatistics.defense++;
            defense.text = playerStatistics.defense.ToString();
            playerStatistics.currentExp -= (int)requirementArray[defenseReq];
            currentExp.text = playerStatistics.currentExp.ToString();
            requirementArray[defenseReq] = requirementArray[defenseReq] * 1.1f;
            defenseReqText.text = expReq + ((int)requirementArray[defenseReq]).ToString();
        }
    }

    private bool CheckExpRequirement(int requirement)
    {
        return playerStatistics.currentExp >= (int)requirementArray[requirement];
    }
}
