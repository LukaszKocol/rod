﻿using System;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static event System.Action<float> OnStartDungeonLevel = delegate { };
    public static event System.Action OntimeOut = delegate { };
    public static event System.Action<int> OnLevelComplete = delegate { };
    public static event System.Action<string> OnUpdateDonksAmount = delegate { };

    public int enemiesCount;
    public float time = 300;
    private bool showShop;
    private int donksAmount;
    // Start is called before the first frame update
    void Awake()
    {
        showShop = false;
        EnemiesChecker.OnLoadShop += ShowShop;
        UIManager.OnHideShop += HideShop;
        PlayerManager.OnPlayerDies += PlayerDies;
        PickUp.OnPickUp += UpdateDonks;
    }

    private void UpdateDonks(int donks)
    {
        donksAmount += donks;
        OnUpdateDonksAmount(donksAmount.ToString());
    }

    private void PlayerDies()
    {
        SceneManager.LoadScene("MainMenu");
    }    
    private void ShowShop()
    {
        showShop = true;
        OnLevelComplete((int)time);
    }
    private void HideShop()
    {
        time = 90;
        OnStartDungeonLevel(time);
        showShop = false;
    }

    private void Start()
    {
        OnStartDungeonLevel(time);
        OnUpdateDonksAmount("0");
    }

    private void OnDestroy()
    {
        EnemiesChecker.OnLoadShop -= ShowShop;
        UIManager.OnHideShop -= HideShop;
        PlayerManager.OnPlayerDies -= PlayerDies;
    }
    // Update is called once per frame
    void Update()
    {
        if (!showShop)
        {
            if (time < 0f)
            {
                OntimeOut();
                SceneManager.LoadScene("MainMenu");
            }
            else
            {
                time -= Time.deltaTime;
            }
        }
    }


}
