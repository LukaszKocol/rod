﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

public class PlayerManager : MonoBehaviour
{
    public static event System.Action<PlayerStatistics> OnLoadStats = delegate { };
    public static event System.Action<PlayerStatistics> OnUpdateHealth = delegate { };
    public static event System.Action OnPlayerDies = delegate { };

    public PlayerStatistics playerStats;
    public int attackAmplification;
    public int penetration;
    public int criticalRate;
    public int criticalDamage;
    public int healthPointsSteal;
    private float healthToRegen = 0;
    private bool inShop = false;

    private void Awake()
    {
        playerStats = new PlayerStatistics();
        EnemyManager.OnDamageTarget += DamagePlayer;
        EnemyStats.OnEnemyDamage += AddExperiance;
        UIManager.OnShowShop += ShowShop;
        UIManager.OnHideShop += HideShop;
        GameManager.OnLevelComplete += RegenHP;
    }

    private void Update()
    {
        if (inShop)
            return;

        if(playerStats.currentHealth < playerStats.maxHealth)
            healthToRegen += playerStats.HPAutoHeal * Time.deltaTime;

        if (healthToRegen > 1)
        {
            playerStats.currentHealth += (int)healthToRegen;

            OnUpdateHealth(playerStats);

            if (playerStats.currentHealth >= playerStats.maxHealth)
                healthToRegen = 0;
            else
                healthToRegen = healthToRegen % 1;
        }   
    }

    private void ShowShop()
    {
        inShop = true;
        OnLoadStats(playerStats);
    }

    private void RegenHP(int timeLeft)
    {
        playerStats.currentHealth += (int)(timeLeft * playerStats.HPAutoHeal);
        if (playerStats.currentHealth > playerStats.maxHealth)
            playerStats.currentHealth = playerStats.maxHealth;
    }

    private void AddExperiance(int expToAdd)
    {
        playerStats.currentExp += expToAdd;
        playerStats.totalExp += expToAdd;
    }

    private void HideShop()
    {
        inShop = false;
    }

    public int CalculateDamage(int targetDefense)
    {
        int additionalAttack;
        if (playerStats.penetration > targetDefense)
        {
            additionalAttack = targetDefense;
            targetDefense = 0;
        }
        else
        {
            targetDefense -= playerStats.penetration;
            additionalAttack = playerStats.penetration;
        }

        int damageToDeal = playerStats.attack + additionalAttack - targetDefense;
        return damageToDeal;
    }

    private void OnDisable()
    {
        EnemyStats.OnEnemyDamage -= AddExperiance;
        EnemyManager.OnDamageTarget -= DamagePlayer;
        UIManager.OnShowShop -= ShowShop;
        UIManager.OnHideShop -= HideShop;
        GameManager.OnLevelComplete -= RegenHP;
    }

    private void OnDestroy()
    {
        EnemyStats.OnEnemyDamage -= AddExperiance;
        EnemyManager.OnDamageTarget -= DamagePlayer;
        UIManager.OnShowShop -= ShowShop;
        UIManager.OnHideShop -= HideShop;
        GameManager.OnLevelComplete -= RegenHP;
    }
    // Start is called before the first frame update
    void Start()
    {
        OnUpdateHealth(playerStats);
    }

    private void DamagePlayer(EnemyStats enemyStats)
    {
        TextRendererParticleSystem.instance.SpawnParticle(transform.position, enemyStats.damage.ToString(), Color.red, 1);
        playerStats.currentHealth -= enemyStats.damage;
        OnUpdateHealth(playerStats);

        if (playerStats.currentHealth <= 0)
            OnPlayerDies();
    }
}
