﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyManager : MonoBehaviour
{
    public static event System.Action<EnemyStats> OnDamageTarget = delegate { };
    public EnemyController enemyController;
    public EnemyStats enemyStats;

    // Update is called once per frame
    void Update()
    {

    }

    public void DeactivateEnemy()
    {
        enemyController.canMove = false;
    }
    public void ActivateEnemy()
    {
        enemyController.canMove = true;
    }

    private void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (enemyController.CheckChaseState())
                OnDamageTarget(enemyStats);
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            enemyController.StartChaseTarget(other.gameObject.transform);
        }
    }
}
