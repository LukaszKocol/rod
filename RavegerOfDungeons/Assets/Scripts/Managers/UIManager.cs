﻿using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Utilities;

public class UIManager : MonoBehaviour
{
    public static event System.Action OnHideShop = delegate { };
    public static event System.Action OnShowShop = delegate { };
    public GameObject gameUI;
    public Slider healthSlider;
    public TMP_Text healthText;
    public TMP_Text timeText;
    public GameObject shop;
    public TMP_Text donksAmount;
    private float time;
    private bool shopView;

    private void Awake()
    {
        shopView = false;
        GameManager.OnStartDungeonLevel += SetTime;
        EnemiesChecker.OnLoadShop += ShowShop;
        PlayerManager.OnUpdateHealth += UpdateHealth;
        GameManager.OnUpdateDonksAmount += UpdateDonks;
    }


    private void OnDisable()
    {
        GameManager.OnStartDungeonLevel -= SetTime;
        EnemiesChecker.OnLoadShop -= ShowShop;
        PlayerManager.OnUpdateHealth -= UpdateHealth;
        GameManager.OnUpdateDonksAmount -= UpdateDonks;
    }
    private void ShowShop()
    {
        shopView = true;
        shop.gameObject.SetActive(true);
        healthText.gameObject.SetActive(false);
        timeText.gameObject.SetActive(false);
        healthSlider.gameObject.SetActive(false);
        gameUI.gameObject.SetActive(false);
        OnShowShop();
    }

    public void HideShop()
    {
        shopView = false;
        shop.gameObject.SetActive(false);
        healthText.gameObject.SetActive(true);
        timeText.gameObject.SetActive(true);
        healthSlider.gameObject.SetActive(true);
        gameUI.gameObject.SetActive(true);
        OnHideShop();
    }

    private void Update()
    {
        if (!shopView)
        {
            UpdateTime();
        }
    }

    public void UpdateHealth(PlayerStatistics playerStats)
    {
        healthSlider.maxValue = playerStats.maxHealth;
        healthSlider.value = playerStats.currentHealth;
        healthText.text = "HEALTH " + playerStats.currentHealth + "/" + playerStats.maxHealth;
    }

    public void SetTime(float time)
    {
        this.time = time;
    }

    public void UpdateTime()
    {
        time -= Time.deltaTime;
        timeText.text = ConvertTimeToString();
    }

    private string ConvertTimeToString()
    {
        string minutes = ((int)(time / 60)).ToString();
        int seconds = (int)(time % 60);
        if (seconds == 0)
        {
            return minutes + ":00";
        }
        else if (seconds < 10)
        {
            return minutes + ":0" + seconds;
        }
        else
        {
            return minutes + ":" + seconds;
        }
    }

    void UpdateDonks(string donks)
    {
        donksAmount.text = donks;
    }

}
