﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Chase : IState
{
    private Rigidbody2D theRB;
    private Vector2 moveDir;
    private Transform targetTransform;
    private Transform objectTransform;
    private float chaseSpeed = 6f;

    public Chase(Rigidbody2D RB)
    {
        moveDir = new Vector2();
        theRB = RB;
    }

    public void OnEnter()
    {
    }

    public void OnExit()
    {
    }

    public void Tick()
    {
        if (targetTransform == null)
            return;

        moveDir = targetTransform.position - objectTransform.position;
        moveDir.Normalize();

        theRB.velocity = moveDir * chaseSpeed;
    }

    internal void SetTransforms(Transform playerTransform, Transform transform)
    {
        targetTransform = playerTransform;
        objectTransform = transform;
    }
}
