﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WaitAfterHit : IState
{
    public float waitAfterHit;
    private float waitTime = 1f;

    public WaitAfterHit()
    {
            
    }
    public void OnEnter()
    {
        waitAfterHit = Random.Range(waitTime * .5f, waitTime * 1.5f);
    }

    public void OnExit()
    {
    }

    public void Tick()
    {
        waitAfterHit -= Time.deltaTime;
    }
}
