﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Idle : IState
{
    private Rigidbody2D theRB;
    public float waitToMove;
    private readonly float waitTime = 1f;
    public Idle(Rigidbody2D theRB)
    {
        this.theRB = theRB;
    }
    public void OnEnter()
    {
        waitToMove = Random.Range(waitTime * .5f, waitTime * 1.5f);
        theRB.velocity = Vector2.zero;
    }

    public void OnExit()
    {
    }

    public void Tick()
    {
        waitToMove -= Time.deltaTime;
    }

}
