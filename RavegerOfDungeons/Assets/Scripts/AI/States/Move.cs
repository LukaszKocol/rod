﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Move : IState
{
    private Rigidbody2D theRB;
    private Vector2 moveDir;
    private readonly float moveTime = 1f;
    public float moveCounter;
    private float moveSpeed;

    public Move(Rigidbody2D RB)
    {
        moveSpeed = 5;
        theRB = RB;
        moveDir = new Vector2();
    }
    public void OnEnter()
    {
        moveCounter = Random.Range(moveTime * .5f, moveTime * 1.5f);
        moveDir.x = Random.Range(-1f, 1f);
        moveDir.y = Random.Range(-1f, 1f);
        moveDir.Normalize();

        theRB.velocity = moveDir * moveSpeed;
    }

    public void OnExit()
    {
    }

    public void Tick()
    {
        moveCounter -= Time.deltaTime;
    }
}
