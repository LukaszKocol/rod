﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Hited : IState
{
    private Rigidbody2D theRB;
    public float waitAfterHited = 0;
    public Hited(Rigidbody2D RB)
    {
        theRB = RB;
    }
    public void OnEnter()
    {
        waitAfterHited = 1f;
        theRB.velocity = theRB.velocity * -3f;
    }

    public void OnExit()
    {
    }

    public void Tick()
    {
        waitAfterHited -= Time.deltaTime;

        theRB.velocity = theRB.velocity * waitAfterHited;
    }
}
