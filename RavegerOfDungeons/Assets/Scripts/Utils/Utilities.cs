﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Utilities
{
    public class Point
    {
        public Point(int posX, int posY)
        {
            x = posX;
            y = posY;
        }

        public int x;
        public int y;
    }

    public class PlayerStatistics
    {
        public PlayerStatistics()
        {
            currentExp = totalExp = 0;
            attack = 30;
            defense = 10;
            currentHealth = maxHealth = 100;
            HPAutoHeal = 1;
            penetration = 5;
        }

        public int currentExp;
        public int totalExp;
        public int attack;
        public int defense;
        public int currentHealth;
        public int maxHealth;
        public float HPAutoHeal;
        public int penetration;
    }
}