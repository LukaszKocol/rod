﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyStats : MonoBehaviour
{
    public static event Action OnEnemyDies = delegate { };
    public static event Action<int> OnEnemyDamage = delegate { };
    public int currentHealth, maxHealth;
    public int experiance, leftExperiance;
    public int defense;
    public int damage;
    public int chanceToDropDonks;
    public GameObject donks;

    // Start is called before the first frame update
    void Start()
    {
        chanceToDropDonks = 50;
        damage = UnityEngine.Random.Range(-2, 3) + 3;
        defense = UnityEngine.Random.Range(-5, 5) + 20;
        currentHealth = maxHealth + UnityEngine.Random.Range(-5, 5);
        leftExperiance = experiance;
    }

    private void OnEnable()
    {
        currentHealth = maxHealth;
        leftExperiance = experiance;
    }

    public void DamageEnemy(int damageToDeal)
    {
        currentHealth -= damageToDeal;

        CalculateExperiance(damageToDeal);

        if(currentHealth <= 0)
        {
            gameObject.SetActive(false);
            OnEnemyDies();
            SpawnLoot();
            currentHealth = 0;
        }
    }

    private void SpawnLoot()
    {
        if (UnityEngine.Random.Range(0, 100) < chanceToDropDonks)
        {
            Instantiate(donks, this.transform.position, Quaternion.identity);
        }
    }

    private void CalculateExperiance(int damageToDeal)
    {
        if (currentHealth > 0)
        {
            int exp = (int)((float)(damageToDeal * experiance) / maxHealth);
            leftExperiance -= exp;
            OnEnemyDamage(exp);
        }
        else
        {
            OnEnemyDamage(leftExperiance);
        }
    }
}
