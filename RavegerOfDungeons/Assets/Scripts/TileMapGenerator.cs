﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Tilemaps;
using Utilities;

public class TileMapGenerator : MonoBehaviour
{
    public static event System.Action<int, int> OnGenerateLevel = (x, y) => { };
    public static event System.Action<int[], int, int, int> OnGeenrateEnemiesPosition = delegate { };

    private int SizeX = 30;
    private int SizeY = 30;
    private const int maxSizeX = 100;
    private const int maxSizeY = 100;
    public Tilemap walls;
    public Tilemap backgroundMap;
    public Tilemap noiseMap;
    public TileBase[] backgroundTileBases;
    public TileBase[] noiseTileBases;
    public TileBase[] wallsTileBases;
    private BoundsInt bounds;
    public int noiseRate;
    private int[] intNoiseMap;
    private int[] intCopyNoiseMap;
    private List<Point> freeSpaces;
    private List<Point> spacesToCheck;
    private List<List<Point>> caveZones;
    // Start is called before the first frame update
    void Start()
    {
        spacesToCheck = new List<Point>();
        caveZones = new List<List<Point>>();
        freeSpaces = new List<Point>();
        intNoiseMap = new int[maxSizeX * maxSizeY];
        intCopyNoiseMap = new int[maxSizeX * maxSizeY];
        noiseRate = 52;
        bounds = new BoundsInt(0, 0, 1, maxSizeX, maxSizeY, 1);
        GenerateMap();
        EnemiesChecker.OnLoadShop += ReGenerateMap;
    }

    private void OnDisable()
    {
        EnemiesChecker.OnLoadShop -= ReGenerateMap;
    }
    private void ReGenerateMap()
    {
        IncrementLevelBounds();
        ClearMap();
        GenerateMap();
    }

    private void IncrementLevelBounds()
    {
        SizeX += 3;
        SizeY += 3;
        if (SizeX > maxSizeX)
            SizeX = maxSizeX;

        if (SizeY > maxSizeY)
            SizeY = maxSizeY;
    }

    private void GenerateMap()
    {
        GenerateNoiseMap();
        NoiseMapIteration();
        NoiseMapIteration();
        AddBorders();
        CheckForUnreachableSpaces();
        AssignToWallsGrid();
        GeneratePlayerStartPosition();
        GenerateEnemiesPosition();
    }

    private void ClearMap()
    {

        spacesToCheck.Clear();
        caveZones.Clear();
        freeSpaces.Clear();

        TileBase[] wallsTiles = walls.GetTilesBlock(bounds);

        for (int x = SizeX; x > 0; x--)
        {
            for (int y = SizeY; y > 0; y--)
            {
                wallsTiles[x + y * bounds.size.y] = null;
            }
        }
        walls.SetTilesBlock(bounds, wallsTiles);
    }

    private void GenerateEnemiesPosition()
    {
        OnGeenrateEnemiesPosition(intCopyNoiseMap, SizeX, SizeY, maxSizeY);
    }

    private void GeneratePlayerStartPosition()
    {
        GenerateSpawnPoint();
    }

    private void GenerateSpawnPoint()
    {
        int x = Random.Range(0, SizeX);
        int y = Random.Range(0, SizeY);
        while (true)
        {
            if (intCopyNoiseMap[x + y * bounds.size.y] == 1)
            {
                OnGenerateLevel(x, y);
                return;
            }

            x = Random.Range(0, SizeX);
            y = Random.Range(0, SizeY);
        }
    }

    private void AddBorders()
    {
        for (int x = 0; x < SizeX; x++)
        {
            for (int y = 0; y < SizeY; y++)
            {
                if (x == 0 || y == 0 || x == SizeX - 1 || y == SizeY - 1)
                    intCopyNoiseMap[x + y * bounds.size.y] = 0;
            }
        }
    }

    private void CheckForUnreachableSpaces()
    {
        for (int x = 0; x < SizeX; x++)
        {
            for (int y = 0; y < SizeY; y++)
            {
                if (intCopyNoiseMap[x + y * bounds.size.y] == 1)
                {
                    freeSpaces.Add(new Point(x,y));
                }
            }
        }

        int currentZone = 0;

        while (freeSpaces.Count != 0)
        {
            caveZones.Add(new List<Point>());
            spacesToCheck.Add(freeSpaces[Random.Range(0, freeSpaces.Count)]);
            freeSpaces.Remove(spacesToCheck[0]);

            while (spacesToCheck.Count != 0)
            {
                caveZones[currentZone].Add(spacesToCheck[0]);
                CheckNeighbors(spacesToCheck[0]);
                spacesToCheck.RemoveAt(0);
            }
            currentZone++;
        }

        int caveSize = caveZones[0].Count;
        int biggestZone = 0;
        for (int i = 1;i < caveZones.Count; i++)
        {
            if (caveSize < caveZones[i].Count)
            {
                biggestZone = i;
                caveSize = caveZones[i].Count;
            }
        }

        for (int i = 0; i < caveZones.Count; i++)
        {
            if (i != biggestZone)
            {
                for (int j = 0; j < caveZones[i].Count; j++)
                {
                    intCopyNoiseMap[caveZones[i][j].x + caveZones[i][j].y * bounds.size.y] = 0;
                }
            }
        }

    }

    private void CheckNeighbors(Point point)
    {
        var neighbor = freeSpaces.Find(p => p.x == point.x + 1 && p.y == point.y);

        if (neighbor != null)
        {
            spacesToCheck.Add(neighbor);
            freeSpaces.Remove(neighbor);
        }

        neighbor = freeSpaces.Find(p => p.x == point.x && p.y == point.y + 1);

        if (neighbor != null)
        {
            spacesToCheck.Add(neighbor);
            freeSpaces.Remove(neighbor);
        }

        neighbor = freeSpaces.Find(p => p.x == point.x - 1 && p.y == point.y);

        if (neighbor != null)
        {
            spacesToCheck.Add(neighbor);
            freeSpaces.Remove(neighbor);
        }

        neighbor = freeSpaces.Find(p => p.x == point.x && p.y == point.y -1);

        if (neighbor != null)
        {
            spacesToCheck.Add(neighbor);
            freeSpaces.Remove(neighbor);
        }
    }

    private void AssignToWallsGrid()
    {
        backgroundMap.SetTilesBlock(bounds, backgroundMap.GetTilesBlock(bounds));
        walls.SetTilesBlock(bounds, walls.GetTilesBlock(bounds));

        TileBase[] backgroundTiles = backgroundMap.GetTilesBlock(bounds);
        TileBase[] wallsTiles = walls.GetTilesBlock(bounds);

        for (int x = 0; x < SizeX; x++)
        {
            for (int y = 0; y < SizeY; y++)
            {
                if (intCopyNoiseMap[x + y * bounds.size.y] == 1)
                {
                    backgroundTiles[x + y * bounds.size.y] = backgroundTileBases[Random.Range(0, backgroundTileBases.Length)];
                    wallsTiles[x + y * bounds.size.y] = null;                }
                else
                {
                    backgroundTiles[x + y * bounds.size.y] = backgroundTileBases[Random.Range(0, backgroundTileBases.Length)];
                    wallsTiles[x + y * bounds.size.y] = wallsTileBases[Random.Range(0, wallsTileBases.Length)];
                }
            }
        }

        backgroundMap.SetTilesBlock(bounds, backgroundTiles);
        walls.SetTilesBlock(bounds, wallsTiles);
        noiseMap.gameObject.SetActive(false);
    }

    private void GenerateNoiseMap()
    {
        int index;

        for (int x = 0; x < SizeX; x++)
        {
            for (int y = 0; y < SizeY; y++)
            {
                index = Random.Range(0, 100) < noiseRate ? 0 : 1;
                intNoiseMap[x + y * bounds.size.y] = index;
                intCopyNoiseMap[x + y * bounds.size.y] = index;
            }
        }

    }

    private void NoiseMapIteration()
    {
        for (int x = 0; x < SizeX; x++)
        {
            for (int y = 0; y < SizeY; y++)
            {
                if (CheckNeighbors(intNoiseMap, x, y))
                {
                    intCopyNoiseMap[x + y * bounds.size.y] = 0;
                }
                else
                {
                    intCopyNoiseMap[x + y * bounds.size.y] = 1;
                }

            }
        }

        for (int j = 0; j < intCopyNoiseMap.Length; j++)
        {
            intNoiseMap[j] = intCopyNoiseMap[j];
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.G) == true)
        {
            GenerateNoiseMap();
        }

        if (Input.GetKeyDown(KeyCode.I) == true)
        {
            NoiseMapIteration();
        }
    }

    private bool CheckNeighbors(int[] intNoiseMap, int posX, int posY)
    {
        int neighborCoutner = 0;

        if (posX + 1 == SizeX || intNoiseMap[posX + 1 + posY * bounds.size.y] == 0)
            neighborCoutner++;

        if ((posX + 1 == SizeX || posY + 1 == SizeY) || intNoiseMap[posX + 1 + (posY + 1) * bounds.size.y] == 0)
            neighborCoutner++;

        if ((posX + 1 == SizeX || posY - 1 < 0) || intNoiseMap[posX + 1 + (posY - 1) * bounds.size.y] == 0)
            neighborCoutner++;

        if (posX - 1 < 0 || intNoiseMap[posX - 1 + posY * bounds.size.y] == 0)
            neighborCoutner++;

        if ((posX - 1 < 0 || posY - 1 < 0) || intNoiseMap[posX - 1 + (posY - 1) * bounds.size.y] == 0)
            neighborCoutner++;

        if ((posX - 1 < 0 || posY + 1 == SizeY) || intNoiseMap[posX - 1 + (posY + 1) * bounds.size.y] == 0)
            neighborCoutner++;

        if (posY - 1 < 0 || intNoiseMap[posX + (posY - 1) * bounds.size.y] == 0)
            neighborCoutner++;

        if (posY + 1 == SizeY || intNoiseMap[posX + (posY + 1) * bounds.size.y] == 0)
            neighborCoutner++;

        return neighborCoutner > 4;
    }
}
